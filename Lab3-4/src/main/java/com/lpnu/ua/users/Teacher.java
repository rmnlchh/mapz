package com.lpnu.ua.users;

import com.lpnu.ua.model.constants.Subjects;
import com.lpnu.ua.model.constants.TeacherRank;
import lombok.Data;

import java.util.List;

@Data
public class Teacher {
    String fullName;
    List<Subjects> subjects;
    TeacherRank rank;

    public Teacher(String fullName, List<Subjects> subjects, TeacherRank rank) {
        this.fullName = fullName;
        this.subjects = subjects;
        this.rank = rank;
    }
}
