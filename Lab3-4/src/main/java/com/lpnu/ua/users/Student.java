package com.lpnu.ua.users;

import com.lpnu.ua.control.Editor;
import com.lpnu.ua.control.Passer;
import com.lpnu.ua.model.Testing;
import com.lpnu.ua.model.tests.*;

public class Student {

    String fullName;
    int age;
    int recentMark;

    public Student(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }

    public void setRecentMark(int recentMark) {
        this.recentMark = recentMark;
    }

    public int getRecentMark() {
        return recentMark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", recentMark=" + recentMark +
                '}';
    }

    public void passTesting(Testing testing) {
        Passer passer = Editor.getPasser();
        for (Test test : testing.tests) {
            if (test instanceof SingleAnswerTest) {
                passer.setYourAnswerForSingleAnswerTest(passer.getRandomAnswer(((SingleAnswerTest) test).getAnswers()), (SingleAnswerTest) test);
            } else if (test instanceof FullAnswerTest) {
                passer.setYourAnswerForFullAnswerTest("I have no idea what to answer :)", (FullAnswerTest) test);
            } else if (test instanceof MultiAnswerTest) {
                passer.setYourAnswerForMultiAnswerTest(passer.getListOfRandomAnswers(((MultiAnswerTest) test).getAnswers()), (MultiAnswerTest) test);
            } else {
                passer.setYourAnswerForComplianceAnswerTest(passer.getRandomCompliance(((ComplianceTest) test).getAnswers()), (ComplianceTest) test);
            }
        }
    }
}
