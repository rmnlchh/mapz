package com.lpnu.ua.model.constants;

public enum EvaluationType {
    BEST_GRADE,
    AVG_GRADE,
    WORST_GRADE
}
