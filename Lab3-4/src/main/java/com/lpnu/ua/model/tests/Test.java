package com.lpnu.ua.model.tests;

import lombok.Data;

@Data
public abstract class Test {
    String question;
    double mark;

    public Test(String question, double mark) {
        this.question = question;
        this.mark = mark;
    }

    public Test() {
        this.question = "";
        this.mark = 0;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }

    public abstract double verify();
}