package com.lpnu.ua.model.constants;

public enum TeacherRank {
    ASSISTANT,
    PRINCIPAL,
    PROFESSOR
}
