package com.lpnu.ua.model.tests;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class ComplianceTest extends Test {
    private Map<String, String> answers;
    private Map<String, String> correctAnswer;
    private Map<String, String> yourAnswer;

    @Override
    public double verify() {
        return correctAnswer.keySet().stream().mapToInt(key -> correctAnswer.get(key).equals(yourAnswer.get(key)) ? 1 : 0).average().orElse(1) * mark;
    }
}
