package com.lpnu.ua.model.constants;

public enum Subjects {
    MATH,
    UA,
    BIOLOGY,
    CHEMISTRY,
    HISTORY,
    PE,
    GEOGRAPHY
}
