package com.lpnu.ua.model;

import com.lpnu.ua.model.constants.EvaluationType;
import com.lpnu.ua.model.tests.Test;
import com.lpnu.ua.users.Teacher;
import lombok.Data;

import java.util.List;

@Data
public class Testing {
    public List<? extends Test> tests;
    Long time;
    Integer numberOfAttempts;
    EvaluationType evaluationType;
    Teacher author;

    public Testing() {
    }

    private Testing(List<? extends Test> tests, Long time, Integer numberOfAttempts, EvaluationType evaluationType, Teacher author) {
        this.tests = tests;
        this.time = time;
        this.numberOfAttempts = numberOfAttempts;
        this.evaluationType = evaluationType;
        this.author = author;
    }

    private Testing(Testing other) {
        this(other.tests, other.time, other.numberOfAttempts, other.evaluationType, other.author);
    }

    public Testing clone() {
        return new Testing(this);
    }

}
