package com.lpnu.ua.model.tests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class FullAnswerTest extends Test {
    private String yourAnswer;

    @Override
    public double verify() {
        return yourAnswer.length() > 20 ? mark : yourAnswer.length() < 10 ? mark / 3 : mark - 5;
    }
}
