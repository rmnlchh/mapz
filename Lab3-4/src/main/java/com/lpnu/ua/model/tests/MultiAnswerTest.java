package com.lpnu.ua.model.tests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class MultiAnswerTest extends Test {
    private List<String> answers;
    private List<String> correctAnswer;
    private List<String> yourAnswer;

    @Override
    public double verify() {
        return correctAnswer.equals(yourAnswer) ? mark : correctAnswer.stream().anyMatch(answer -> yourAnswer.contains(answer)) ? mark / 2 : 0;
    }
}
