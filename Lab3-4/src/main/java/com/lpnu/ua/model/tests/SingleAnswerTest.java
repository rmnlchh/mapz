package com.lpnu.ua.model.tests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class SingleAnswerTest extends Test {
    private List<String> answers;
    private String correctAnswer;
    private String yourAnswer;

    @Override
    public double verify() {
        return correctAnswer.equals(yourAnswer) ? mark : mark - 1.5;
    }
}
