package com.lpnu.ua;

import com.lpnu.ua.control.Editor;
import com.lpnu.ua.control.TestingSystem;
import com.lpnu.ua.model.Testing;
import com.lpnu.ua.model.tests.Test;
import com.lpnu.ua.model.constants.Subjects;
import com.lpnu.ua.users.Student;
import com.lpnu.ua.users.Teacher;
import com.lpnu.ua.model.constants.TeacherRank;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<Subjects> subjects = new ArrayList<>();
        subjects.add(Subjects.MATH);
        Teacher teacher = new Teacher("Roman Shkrab", subjects, TeacherRank.ASSISTANT);

        Student student = new Student("Roman Lechyha", 18);

        TestingSystem sys = new TestingSystem();
        Testing testing = sys.setUp(teacher);

        student.passTesting(testing);

        System.out.println(testing);
        System.out.println();

        sys.autoCheck(student, testing);

        System.out.println(student);
        System.out.println();

        double percentage = student.getRecentMark() / (testing.tests.stream().mapToDouble(Test::getMark)).sum();

        System.out.printf("The student scored %d%% points in the testing!", (int) (percentage * 100));
    }
}
