package com.lpnu.ua.control;

import com.lpnu.ua.model.Testing;
import com.lpnu.ua.model.constants.EvaluationType;
import com.lpnu.ua.model.tests.SingleAnswerTest;
import com.lpnu.ua.model.tests.Test;
import com.lpnu.ua.users.Student;
import com.lpnu.ua.users.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TestingSystem {
    public Testing setUp(Teacher teacher) {
        String log = "";
        Testing testing = new Testing();
        List<Test> tests = new ArrayList<>();

        Editor editor = Editor.getInstance();
        editor.setAuthor(teacher, testing);
        log += "-----------AUTHOR WAS ADDED-----------\n" + teacher.toString();

        editor.setEvaluationType(EvaluationType.AVG_GRADE, testing);
        log += "\n-----------EVALUATION TYPE WAS ADDED-----------\n" + EvaluationType.AVG_GRADE.toString();

        editor.setNumberOfAttempts(1, testing);
        log += "\n-----------NUMBER OF ATTEMPTS WAS ADDED-----------\n1\n";
        editor.setTime(1200L, testing);
        log += "-----------TIME WAS ADDED-----------\n1200 seconds\n";

        List<String> answers = new ArrayList<>();
        answers.add("Builder");
        answers.add("Observer");
        answers.add("Singleton");
        answers.add("Memento");

        Test sat = editor.getCreator().createSAT(editor, "Which pattern ruins SOLID?", answers, "Singleton", 3);
        tests.add(sat);

        log += "-----------TEST WAS ADDED-----------\n" + sat.toString();

        answers.set(2, "Decorator");
        answers.set(1, "Cascade");

        List<String> matAnswers = new ArrayList<String>() {
            {
                add(answers.get(1));
                add(answers.get(2));
            }
        };
        Test mat = editor.getCreator().createMAT(editor, "Which of these are structure patterns?", answers, matAnswers, 5);
        tests.add(mat);

        log += "\n-----------TEST WAS ADDED-----------\n" + mat.toString();

        Test fat = editor.getCreator().createFAT(editor, "Purpose of life?", 8);
        tests.add(fat);

        log += "\n-----------TEST WAS ADDED-----------\n" + fat.toString();
        testing.setTests(tests);

        log+="\n-----------END OF THE LOG-----------\n";
        System.out.println(log);
        System.out.println();

        return testing;
    }

    public void autoCheck(Student student, Testing testing) {
        student.setRecentMark((int) Math.ceil(Editor.getExaminer().checkTesting(testing)));
    }

}
