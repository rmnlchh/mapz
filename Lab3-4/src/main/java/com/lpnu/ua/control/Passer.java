package com.lpnu.ua.control;

import com.lpnu.ua.model.tests.ComplianceTest;
import com.lpnu.ua.model.tests.FullAnswerTest;
import com.lpnu.ua.model.tests.MultiAnswerTest;
import com.lpnu.ua.model.tests.SingleAnswerTest;

import java.util.*;

public class Passer {
    public void setYourAnswerForSingleAnswerTest(String yourAnswer, SingleAnswerTest singleAnswerTest) {
        singleAnswerTest.setYourAnswer(yourAnswer);
    }

    public void setYourAnswerForMultiAnswerTest(List<String> yourAnswer, MultiAnswerTest multiAnswerTest) {
        multiAnswerTest.setYourAnswer(yourAnswer);
    }

    public void setYourAnswerForFullAnswerTest(String yourAnswer, FullAnswerTest fullAnswerTest) {
        fullAnswerTest.setYourAnswer(yourAnswer);
    }

    public void setYourAnswerForComplianceAnswerTest(Map<String, String> yourAnswer, ComplianceTest complianceTest) {
        complianceTest.setYourAnswer(yourAnswer);
    }

    public String getRandomAnswer(List<String> answers) {
        return answers.get(new Random().nextInt(answers.size()));
    }

    public List<String> getListOfRandomAnswers(Collection<String> answers) {
        List<String> yourAnswers = new ArrayList<>(answers);
        Collections.shuffle(yourAnswers);
        int upperBound = 1 + new Random().nextInt(answers.size() - 1);
        return yourAnswers.subList(0, upperBound);
    }

    public Map<String, String> getRandomCompliance(Map<String, String> answers) {
        List<String> keys = getListOfRandomAnswers(answers.keySet());
        List<String> values = getListOfRandomAnswers(answers.values());
        return new HashMap<String, String>() {
            {
                for (int i = 0; i < keys.size(); i++) {
                    put(keys.get(i), values.get(i));
                }
            }
        };
    }
}
