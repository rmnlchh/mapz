package com.lpnu.ua.control;

import com.lpnu.ua.model.constants.EvaluationType;
import com.lpnu.ua.model.Testing;
import com.lpnu.ua.model.tests.*;
import com.lpnu.ua.users.Teacher;

import java.util.List;
import java.util.Map;

public class Editor {
    static Editor instance;
    static Passer passer;
    static Examiner examiner;
    static Creator creator;

    public static Editor getInstance() {
        if (instance == null) {
            instance = new Editor();
            passer = new Passer();
            examiner = new Examiner();
            creator = new Creator();
        }
        return instance;
    }

    public static Passer getPasser() {
        return passer;
    }

    public static Examiner getExaminer() {
        return examiner;
    }

    public Creator getCreator() {
        return creator;
    }

    private Editor() {

    }

    public void setTests(List<Test> tests, Testing testing) {
        testing.setTests(tests);
    }

    public void setTime(Long time, Testing testing) {
        testing.setTime(time);
    }

    public void setNumberOfAttempts(Integer numberOfAttempts, Testing testing) {
        testing.setNumberOfAttempts(numberOfAttempts);
    }

    public void setEvaluationType(EvaluationType evaluationType, Testing testing) {
        testing.setEvaluationType(evaluationType);
    }

    public void setAuthor(Teacher teacher, Testing testing) {
        testing.setAuthor(teacher);
    }

    public void setQuestion(String question, Test test) {
        test.setQuestion(question);
    }

    public void setMark(double mark, Test test) {
        test.setMark(mark);
    }

    public void deleteTest(Testing testing, Test test) {
        testing.getTests().remove(test);
    }

    public void setAnswerForSingleAnswerTest(List<String> answers, String correctAnswer, SingleAnswerTest test) {
        test.setAnswers(answers);
        test.setCorrectAnswer(correctAnswer);
    }

    public void setAnswerForMultiAnswerTest(List<String> answers, List<String> correctAnswers, MultiAnswerTest test) {
        test.setAnswers(answers);
        test.setCorrectAnswer(correctAnswers);
    }

    public void setAnswersForMultiAnswerTest(List<String> answers, List<String> correctAnswers, MultiAnswerTest test) {
        test.setAnswers(answers);
        test.setCorrectAnswer(correctAnswers);
    }

    public void setAnswerForFullAnswerTest(String answer, FullAnswerTest test) {
        test.setYourAnswer(answer);
    }

    public void setAnswersForComplianceAnswerTest(Map<String, String> answers, Map<String, String> correctAnswer,
                                                  Map<String, String> yourAnswer, ComplianceTest test) {
        test.setAnswers(answers);
        test.setCorrectAnswer(correctAnswer);
        test.setYourAnswer(yourAnswer);
    }

}