package com.lpnu.ua.control;

import com.lpnu.ua.factory.FullAnswerTestFactory;
import com.lpnu.ua.factory.MultiAnswerTestFactory;
import com.lpnu.ua.factory.SingleAnswerTestFactory;
import com.lpnu.ua.factory.TestFactory;
import com.lpnu.ua.model.tests.MultiAnswerTest;
import com.lpnu.ua.model.tests.SingleAnswerTest;
import com.lpnu.ua.model.tests.Test;

import java.util.ArrayList;
import java.util.List;

public class Creator {
    public Test createSAT(Editor editor, String questiton, List<String> answers, String correctAnswer, int mark) {

        TestFactory factory = new SingleAnswerTestFactory();
        Test singleAnswerTest = factory.createTest();
        editor.setMark(3, singleAnswerTest);
        editor.setQuestion(questiton, singleAnswerTest);

        editor.setAnswerForSingleAnswerTest(new ArrayList<>(answers), correctAnswer, (SingleAnswerTest) singleAnswerTest);

        return singleAnswerTest;
    }

    public Test createMAT(Editor editor, String question, List<String> answers, List<String> correctAnswers, int mark) {

        TestFactory factory = new MultiAnswerTestFactory();
        Test multiAnswerTest = factory.createTest();

        editor.setMark(mark, multiAnswerTest);
        editor.setQuestion(question, multiAnswerTest);

        editor.setAnswerForMultiAnswerTest(new ArrayList<>(answers), correctAnswers, (MultiAnswerTest) multiAnswerTest);

        return multiAnswerTest;
    }

    public Test createFAT(Editor editor, String question, int mark) {

        TestFactory factory = new FullAnswerTestFactory();
        Test fullAnswerTest = factory.createTest();
        editor.setMark(mark, fullAnswerTest);
        editor.setQuestion(question, fullAnswerTest);

        return fullAnswerTest;
    }
}
