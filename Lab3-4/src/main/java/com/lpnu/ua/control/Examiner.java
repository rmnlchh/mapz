package com.lpnu.ua.control;

import com.lpnu.ua.model.Testing;
import com.lpnu.ua.model.tests.Test;

public class Examiner {
    public double checkTesting(Testing testing) {
        return testing.tests.stream().mapToDouble(Test::verify).sum();
    }
}
