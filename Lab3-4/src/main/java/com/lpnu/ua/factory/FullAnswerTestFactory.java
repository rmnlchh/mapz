package com.lpnu.ua.factory;

import com.lpnu.ua.model.tests.FullAnswerTest;
import com.lpnu.ua.model.tests.Test;

public class FullAnswerTestFactory implements TestFactory {
    @Override
    public Test createTest() {
        return new FullAnswerTest();
    }
}

