package com.lpnu.ua.factory;

import com.lpnu.ua.model.tests.MultiAnswerTest;
import com.lpnu.ua.model.tests.Test;

public class MultiAnswerTestFactory implements TestFactory {
    @Override
    public Test createTest() {
        return new MultiAnswerTest();
    }
}
