package com.lpnu.ua.factory;

import com.lpnu.ua.model.tests.SingleAnswerTest;

public class SingleAnswerTestFactory implements TestFactory {
    @Override
    public SingleAnswerTest createTest() {
        return new SingleAnswerTest();
    }
}
