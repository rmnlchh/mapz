package com.lpnu.ua.factory;

import com.lpnu.ua.model.tests.Test;

public interface TestFactory {
    Test createTest();
}
