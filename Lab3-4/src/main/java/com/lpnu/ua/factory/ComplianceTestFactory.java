package com.lpnu.ua.factory;

import com.lpnu.ua.model.tests.ComplianceTest;
import com.lpnu.ua.model.tests.Test;

public class ComplianceTestFactory implements TestFactory {

    @Override
    public Test createTest() {
        return new ComplianceTest();
    }
}
